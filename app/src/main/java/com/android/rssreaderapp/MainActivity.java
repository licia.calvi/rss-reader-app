package com.android.rssreaderapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtnViewRssFeeds(View v) {
        Intent intent = new Intent(getApplicationContext(),ActivityViewRSSFeed.class);
        EditText editText = (EditText)findViewById(R.id.editTextFeedUrl);
        intent.putExtra("feedUrl",editText.getText().toString());
        startActivity(intent);
    }

    public void onClickBtnBookmarks(View v) {
        startActivity(new Intent(getApplicationContext(),ActivityBookmark.class));
    }
}