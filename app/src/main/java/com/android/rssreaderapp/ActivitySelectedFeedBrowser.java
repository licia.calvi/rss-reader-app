package com.android.rssreaderapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

public class ActivitySelectedFeedBrowser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_browser);
        WebView webView = (WebView) findViewById(R.id.webViewBrowser);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Intent feedIntent = getIntent();
        Bundle bundle = feedIntent.getExtras();

        String title = (String)bundle.get("title");
        textViewTitle.setText(title);

        String feedLink = (String)bundle.get("feedLink");
        webView.loadUrl(feedLink);
    }
}