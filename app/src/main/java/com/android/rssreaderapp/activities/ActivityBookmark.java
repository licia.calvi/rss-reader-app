package com.android.rssreaderapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.rssreaderapp.R;
import com.android.rssreaderapp.contentprovider.BookmarkProvider;
import com.android.rssreaderapp.other.RssItem;
import com.android.rssreaderapp.other.RssItemAdapter;

import java.util.ArrayList;

public class ActivityBookmark extends AppCompatActivity {

    private ListView bookmarkListView;
    private ArrayList<RssItem> rssItemList;
    private ArrayAdapter<RssItem> newsArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("ACTIVITY_BOOKMARK", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);

        bookmarkListView = findViewById(R.id.bookmarkListView);

        //rssItemList = (ArrayList<RssItem>)getIntent().getExtras().get("rssList");

        updateListView();
        bookmarkListView.setOnItemClickListener((parent, view, position, id) -> {
            RssItem rssItem = (RssItem)parent.getItemAtPosition(position);
            Intent feedIntent = new Intent(getApplicationContext(),ActivitySelectedBookmarkBrowser.class);
            feedIntent.putExtra("title", rssItem.getTitle());
            //feedIntent.putExtra("description", rssItem.getDescription());
            feedIntent.putExtra("feedLink", rssItem.getLink());
            startActivity(feedIntent);
        });

    }

    private void updateListView() {
        Cursor cursor = getContentResolver().query(BookmarkProvider.CONTENT_URI,null,null,null,"_id");
        if(rssItemList == null){
            rssItemList =  new ArrayList<>();
        }else{
            rssItemList.clear();
        }

        while(cursor.moveToNext()){
            RssItem item = new RssItem();
            //int id = cursor.getInt(0);
            item.setTitle(cursor.getString(1));
            item.setDescription(cursor.getString(2));
            item.setLink(cursor.getString(3));
            rssItemList.add(item);
        }

        newsArrayAdapter = new ArrayAdapter<RssItem>(this, android.R.layout.simple_list_item_1, rssItemList);
        newsArrayAdapter = new RssItemAdapter(this, android.R.layout.simple_list_item_1, rssItemList);
        bookmarkListView.setAdapter(newsArrayAdapter);
    }

    @Override
    protected void onPause() {
        Log.d("ACTIVITY_BOOKMARK", "onPause");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        Log.d("ACTIVITY_BOOKMARK", "onRestart");
        super.onRestart();
        updateListView();
    }
}