package com.android.rssreaderapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.rssreaderapp.contentprovider.BookmarkProvider;
import com.android.rssreaderapp.R;
import com.android.rssreaderapp.other.RssItem;

import java.util.ArrayList;

public class ActivitySelectedFeedBrowser extends AppCompatActivity {

    ContentValues contentValues = new ContentValues();
    private String title;
    private String description;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_feed_browser);
        WebView webView = (WebView) findViewById(R.id.webViewBrowser);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Intent feedIntent = getIntent();
        Bundle bundle = feedIntent.getExtras();

        title = (String)bundle.get("title");
        description = (String)bundle.get("description");
        link = (String)bundle.get("feedLink");

        textViewTitle.setText(title);
        webView.loadUrl(link);
    }

    public void btnOnClickAddToBookmark(View view) {
        contentValues.put("title",title);
        contentValues.put("description",description);
        contentValues.put("link",link);

        getContentResolver().insert(BookmarkProvider.CONTENT_URI,contentValues);
        Toast.makeText(this,"Feed added to bookmark",Toast.LENGTH_SHORT).show();
    }
}