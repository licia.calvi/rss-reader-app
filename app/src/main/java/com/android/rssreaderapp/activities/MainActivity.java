package com.android.rssreaderapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.rssreaderapp.R;
import com.android.rssreaderapp.contentprovider.BookmarkProvider;
import com.android.rssreaderapp.other.RssItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtnViewRssFeeds(View v) {
        Intent intent = new Intent(getApplicationContext(),ActivityViewRSSFeed.class);
        EditText editText = (EditText)findViewById(R.id.editTextFeedUrl);
        intent.putExtra("feedUrl",editText.getText().toString());
        startActivity(intent);
    }

    public void onClickBtnBookmarks(View v) {
        /*Cursor cursor = getContentResolver().query(BookmarkProvider.CONTENT_URI,null,null,null,"_id");

        Intent feedIntent = new Intent(getApplicationContext(),ActivityBookmark.class);
        ArrayList<RssItem> rssList =  new ArrayList<>();

        while(cursor.moveToNext()){
            RssItem item = new RssItem();
            //int id = cursor.getInt(0);
            item.setTitle(cursor.getString(1));
            item.setDescription(cursor.getString(2));
            item.setLink(cursor.getString(3));
            rssList.add(item);
        }

        feedIntent.putExtra("rssList",rssList);*/

        Intent feedIntent = new Intent(getApplicationContext(),ActivityBookmark.class);
        startActivity(feedIntent);
    }

}