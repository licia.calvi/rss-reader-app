package com.android.rssreaderapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.rssreaderapp.R;
import com.android.rssreaderapp.service.RetrieveFeedService;
import com.android.rssreaderapp.other.RssItem;
import com.android.rssreaderapp.other.RssItemAdapter;

import java.util.ArrayList;

public class ActivityViewRSSFeed extends AppCompatActivity {

    private ListView newsListView;
    private ArrayList<RssItem> rssItemList;
    private ArrayAdapter<RssItem> newsArrayAdapter;

    private IntentFilter intentFilter;

    private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //overwrites the RssList
            ArrayList<RssItem> tempRssList =  (ArrayList<RssItem>)intent.getSerializableExtra("stories");
            if (tempRssList != null) {
                rssItemList.clear();
                rssItemList.addAll(tempRssList);
                newsArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_rssfeed);

        newsListView = findViewById(R.id.feedListView);

        //initialize ListView and corresponding array adapter
        rssItemList = new ArrayList<RssItem>();
        newsArrayAdapter = new ArrayAdapter<RssItem>(this, android.R.layout.simple_list_item_1, rssItemList);
        newsArrayAdapter = new RssItemAdapter(this, android.R.layout.simple_list_item_1, rssItemList);
        newsListView.setAdapter(newsArrayAdapter);

        newsListView.setOnItemClickListener((parent, view, position, id) -> {
            RssItem rssItem = (RssItem)parent.getItemAtPosition(position);
            Intent feedIntent = new Intent(getApplicationContext(),ActivitySelectedFeedBrowser.class);
            feedIntent.putExtra("title", rssItem.getTitle());
            feedIntent.putExtra("description", rssItem.getDescription());
            feedIntent.putExtra("feedLink", rssItem.getLink());
            /*Intent feedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rssItem.getLink()));*/
            startActivity(feedIntent);
        });

        //register receiver
        intentFilter = new IntentFilter();
        intentFilter.addAction("RSS_RETRIEVED");
        registerReceiver(intentReceiver,intentFilter);

        Intent serviceIntent = new Intent(this, RetrieveFeedService.class);
        String url = (String)getIntent().getExtras().get("feedUrl");
        serviceIntent.putExtra("feedUrl",url);
        //start service
        startService(serviceIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(intentReceiver);
    }
}