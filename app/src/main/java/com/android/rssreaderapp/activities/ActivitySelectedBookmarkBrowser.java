package com.android.rssreaderapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.rssreaderapp.R;
import com.android.rssreaderapp.contentprovider.BookmarkProvider;

public class ActivitySelectedBookmarkBrowser extends AppCompatActivity {

    ContentValues contentValues = new ContentValues();
    private String title;
    private String description;
    private String link;
    private Bundle bundleSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_bookmark_browser);

        WebView webView = (WebView) findViewById(R.id.webView);
        TextView textViewTitle = (TextView) findViewById(R.id.textViewBookmarkTitle);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Intent feedIntent = getIntent();
        bundleSelected = feedIntent.getExtras();

        title = (String)bundleSelected.get("title");
        //description = (String)bundle.get("description");
        link = (String)bundleSelected.get("feedLink");

        textViewTitle.setText(title);
        webView.loadUrl(link);
    }

    public void btnOnClickRemoveFromBookmark(View view) {
        //Uri uri, String selection, String[] selectionArgs
        getContentResolver().delete(BookmarkProvider.CONTENT_URI,"title=?",new String[]{title});
        Toast.makeText(this,"Feed removed from bookmark",Toast.LENGTH_SHORT).show();
    }


}