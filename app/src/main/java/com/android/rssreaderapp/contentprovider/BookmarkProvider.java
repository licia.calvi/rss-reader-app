package com.android.rssreaderapp.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BookmarkProvider extends ContentProvider {

    private static final String DB_NAME = "bookmark";
    private static final String DB_TABLE = "feeds";
    private static final int DB_VER = 1;

    private SQLiteDatabase myDb;
    public static final String AUTHORITY = "com.android.rss.bookmark";
    public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/feeds");
    static int FEED = 1;
    static int FEED_ID = 2;

    static UriMatcher myUri = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        myUri.addURI(AUTHORITY,"feeds",FEED);
        //# => particular rowId
        myUri.addURI(AUTHORITY, "feeds/#",FEED_ID);
    }

    class MyDatabase extends SQLiteOpenHelper {

        public MyDatabase(Context context) {
            super(context, DB_NAME,null,DB_VER);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("create table " + DB_TABLE + " " +
                    "(_id integer primary key autoincrement, title text, description text, link text)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            sqLiteDatabase.execSQL("drop table if exists "+DB_TABLE);
        }
    }

    public BookmarkProvider() {
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return myDb.delete(DB_TABLE,selection,selectionArgs);

    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long row = myDb.insert(DB_TABLE,null,values);
        if(row > 0){
            uri = ContentUris.withAppendedId(CONTENT_URI,row);
            getContext().getContentResolver().notifyChange(uri,null);
        }
        return uri;
    }

    @Override
    public boolean onCreate() {
        MyDatabase myDatabase = new MyDatabase(getContext());
        myDb = myDatabase.getWritableDatabase();
        if(myDb != null){
            return true;
        }
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder myQuery = new SQLiteQueryBuilder();
        myQuery.setTables(DB_TABLE);
        Cursor cursor = myQuery.query(myDb,null,null,null,null,null,"_id");
        cursor.setNotificationUri(getContext().getContentResolver(),uri);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }


}